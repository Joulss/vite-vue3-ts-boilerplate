import { defineStore } from 'pinia';
import { Answers } from '@/types/Answers';

export const useStore = defineStore('myStupidFriendStore', {
  state: () => ({
    stupidAnswers: {
      toQuestions: [
        'Ouais...',
        'Possible',
        'Ben non',
        'Hmm...',
        'Je sais pas...',
        'Va savoir',
        'Commence par te demander pourquoi tu as posé cette question !',
        'Question difficile... Comme disait Kant : Nous ne connaissons a priori des choses que ce que nous y mettons nous-mêmes.',
        '42 évidemment',
        'La réponse est dans la question'
      ],
      toSentences: [
        'Absolument.',
        'C\'est ce que je pense aussi...',
        'C\'est toi qui le dis',
        'Faut voir',
        'Tout à fait'
      ]
    } as Answers,
    userName       : '',
    questionsCount : 0
  }),

  actions: {
    storeUserName(userName: string): void {
      this.userName = userName;
    },
    incrementQuestionsCounter(): void {
      this.questionsCount++;
    }
  },

  getters: {
  }
});
