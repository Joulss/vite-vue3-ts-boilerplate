export interface DialogItem {
  id: number
  time: string
  value: string
}
