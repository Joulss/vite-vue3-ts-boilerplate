export interface Question {
  type: 'question' | 'sentence'
  value: string
}

