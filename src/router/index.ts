import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';

const routes = [
  {
    path      : '/',
    name      : 'home',
    meta      : {},
    component : () => import('@/views/HomePage.vue')
  }
] as RouteRecordRaw[];

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes
});

export default router;
