import * as path from 'path';
import vue from '@vitejs/plugin-vue';
import { defineConfig, UserConfigExport } from 'vite';

export default (): UserConfigExport => {
  return defineConfig({
    root    : '.',
    base    : '/',
    plugins : [
      vue()
    ],
    build: {
      outDir    : './dist',
      sourcemap : true
    },
    resolve: {
      mainFields : ['module'],
      alias      : {
        '@': path.resolve(__dirname, './src')
      }
    },
    server: {
      port: 3000
    }
  });
};
