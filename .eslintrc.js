module.exports = {
  env: {
    node: true
  },
  globals: {
    NodeJS : 'readonly',
    cy     : true
  },
  plugins: [
    '@typescript-eslint',
    'import-newlines'
  ],
  extends: [
    'eslint:recommended',
    'plugin:vue/vue3-recommended',
    '@vue/typescript/recommended',
    'plugin:@typescript-eslint/recommended'
  ],
  parser        : 'vue-eslint-parser',
  parserOptions : {
    parser     : '@typescript-eslint/parser',
    sourceType : 'module'
  },
  root  : true,
  rules : {
    '@typescript-eslint/ban-ts-comment' : 'off',
    'array-bracket-spacing'             : ['error', 'never'],
    'arrow-parens'                      : ['error', 'as-needed'],
    'brace-style'                       : ['error', '1tbs', { allowSingleLine: false }],
    'comma-dangle'                      : ['error', 'never'],
    'comma-spacing'                     : ['error', {
      before : false,
      after  : true
    }],
    curly                     : ['error', 'all'],
    'import-newlines/enforce' : ['error', 1000, 1000000],
    indent                    : ['error', 2],
    'key-spacing'             : ['error', {
      multiLine: {
        beforeColon : false,
        afterColon  : true
      },
      align: {
        beforeColon : true,
        afterColon  : true,
        on          : 'colon'
      }
    }],
    'no-multi-spaces': ['error', {
      exceptions: {
        Property             : true,
        ImportDeclaration    : true,
        AssignmentExpression : true,
        VariableDeclarator   : true
      }
    }],
    'no-var'                  : 'error',
    'object-curly-spacing'    : ['error', 'always'],
    'object-property-newline' : ['error', {
      allowAllPropertiesOnSameLine: true
    }],
    'prefer-const' : 'error',
    'quote-props'  : ['error', 'as-needed'],
    quotes         : ['error', 'single'],
    semi           : ['error', 'always'],
    'sort-imports' : ['warn', {
      ignoreCase            : true,
      ignoreDeclarationSort : true
    }],
    'space-before-blocks'           : ['error', 'always'],
    'space-before-function-paren'   : ['error', 'never'],
    'space-infix-ops'               : 'error',
    'vue/first-attribute-linebreak' : ['error', {
      singleline : 'beside',
      multiline  : 'beside' }],
    'vue/html-closing-bracket-newline': ['error', {
      singleline : 'never',
      multiline  : 'never'
    }],
    'vue/html-indent': ['error', 2, {
      attribute                 : 1,
      baseIndent                : 1,
      closeBracket              : 0,
      alignAttributesVertically : true,
      ignores                   : []
    }],
    'vue/html-self-closing': ['error', {
      html: {
        void      : 'always',
        normal    : 'never',
        component : 'always'
      }
    }],
    'vue/multiline-html-element-content-newline' : 'off',
    'vue/require-default-prop'                   : 'off',
    'vue/script-indent'                          : ['error', 2, {
      baseIndent: 1
    }],
    'vue/singleline-html-element-content-newline' : 'off',
    '@typescript-eslint/type-annotation-spacing'  : 'error',
    '@typescript-eslint/member-ordering'          : ['warn', {
      default: {
        order: 'natural-case-insensitive'
      }
    }],
    '@typescript-eslint/no-empty-interface'    : 'off',
    '@typescript-eslint/no-non-null-assertion' : 'off',
    '@typescript-eslint/no-explicit-any'       : 'off'
  },
  overrides: [
    {
      files : ['*.vue'],
      rules : {
        indent: 'off'
      }
    }
  ]
};
